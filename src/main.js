import {createApp} from 'vue'
import App from './App.vue'
import './assets/main.css'
import VueSidebarMenu from 'vue-sidebar-menu'
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'
import router from '../src/router/'
import store from './store'

const app = createApp(App);

app.use(VueSidebarMenu);
app.use(router);
app.use(store);
app.mount('#app');
