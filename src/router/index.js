import { createRouter, createWebHistory } from 'vue-router'
import UsersList from "../components/UsersList";
import FavouritesList from "../components/FavouritesList";
import UserDetails from "../components/UserProfile";

const routes = [
  {
    path: '/',
    name: 'UsersList',
    component: UsersList
  },
  {
    path: '/users',
    name: 'Users',
    component: UsersList
  },
  {
    path: '/users/:id',
    name: 'UserDetails',
    component: UserDetails,
    props: true,
  },
  {
    path: '/favourites',
    name: 'Fa',
    component: FavouritesList
  },
];

export default createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

