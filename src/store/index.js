import {createStore} from "vuex";

export default createStore({
  state () {
    return {
      users: [],
      favourites: [],
      search: '',
      user: {},
    }
  },
  mutations: {
    favouritesChange (state, payload) {
      if (!payload.length){
        localStorage.removeItem('favourites');
      } else {
        localStorage.setItem('favourites', JSON.stringify([...payload]));
      }
      state.favourites = [...payload];
    },
    usersChange (state, payload) {
      state.users = [...payload];
    },
    userChange (state, payload) {
      state.user = payload;
    },
    searchChange (state, payload) {
      state.search = payload;
    },

  }
});
